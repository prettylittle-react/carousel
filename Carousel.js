import React from 'react';
import PropTypes from 'prop-types';

//import Editable from 'Editable';
//import Text from 'text';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import {getModifiers} from 'libs/component';

// import Shapes from 'libs/SVG';
const Shapes = {
	CHEVERON_LEFT: 'M20,50l50,50l10-10L40,50l40-40L70,0L20,50z'
};

import './Carousel.scss';

// https://unpkg.com/flickity-hash@1.0.3/hash.js

let Flickity = null;

if (!SERVER) {
	Flickity = require('flickity-imagesloaded');

	Flickity.prototype._createResizeClass = function() {
		const _this = this;

		setTimeout(() => {
			_this.element.classList.add('flickity-resize');
		}, 500);
	};

	Flickity.createMethods.push('_createResizeClass');

	const resize = Flickity.prototype.resize;

	Flickity.prototype.resize = function() {
		this.element.classList.remove('flickity-resize');
		resize.call(this);
		this.element.classList.add('flickity-resize');
	};

	// Hash navigation
	const proto = Flickity.prototype;

	Flickity.createMethods.push('_createHash');

	proto._createHash = function() {
		if (!this.options.hash) {
			return;
		}
		this.connectedHashLinks = [];
		// hash link listener
		// use HTML5 history pushState to prevent page scroll jump
		this.onHashLinkClick = function(event) {
			event.preventDefault();
			this.selectCell(event.currentTarget.hash);
			history.replaceState(null, '', event.currentTarget.hash);
		}.bind(this);

		// events
		this.on('activate', this.activateHash);
		this.on('deactivate', this.deactivateHash);
	};

	proto.activateHash = function() {
		this.on('change', this.onChangeHash);
		// overwrite initialIndex
		if (this.options.initialIndex === undefined && location.hash) {
			var cell = this.queryCell(location.hash);
			if (cell) {
				this.options.initialIndex = this.getCellSlideIndex(cell);
				//setTimeout(()=>{this.dispatchEvent( 'select', null, [ this.options.initialIndex ] );},100)
			}
		}

		this.connectHashLinks();
	};

	proto.deactivateHash = function() {
		this.off('change', this.onChangeHash);
		this.disconnectHashLinks();
	};

	proto.onChangeHash = function() {
		var id = this.selectedElement.id;
		if (id) {
			var url = '#' + id;
			history.replaceState(null, '', url);
		}
	};

	proto.connectHashLinks = function() {
		var links = document.querySelectorAll('a');
		for (var i = 0; i < links.length; i++) {
			this.connectHashLink(links[i]);
		}
	};

	// used to test if link is on same page
	var proxyLink = document.createElement('a');

	proto.connectHashLink = function(link) {
		if (!link.hash) {
			return;
		}
		// check that link is for the same page
		proxyLink.href = link.href;
		if (proxyLink.pathname != location.pathname) {
			return;
		}
		var cell = this.queryCell(link.hash);
		if (!cell) {
			return;
		}
		link.addEventListener('click', this.onHashLinkClick);
		this.connectedHashLinks.push(link);
	};

	proto.disconnectHashLinks = function() {
		this.connectedHashLinks.forEach(function(link) {
			link.removeEventListener('click', this.onHashLinkClick);
		}, this);
		this.connectedHashLinks = [];
	};
}

/**
 * Carousel
 * @description [description]
 * @example
  <div id="Carousel"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Carousel, {
	}), document.getElementById("Carousel"));
  </script>
 */
class Carousel extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'slider';

		this.state = {
			dots: 0,
			currentSlide: 0
		};

		const defaults = {
			hash: false,
			prevNextButtons: true,
			pageDots: true,
			imagesLoaded: true,
			wrapAround: true,
			resize: true,
			watchCSS: false
		};

		const options = {...defaults, ...props.options};

		options.cellSelector = `.${this.baseClass}__cell`;

		this.custom = {
			prevNextButtons: options.prevNextButtons,
			pageDots: options.pageDots
		};

		if (props.hasCustomControls) {
			options.prevNextButtons = false;
			options.pageDots = false;
		}

		// TODO: flag this
		this.id = props.id || 'ads';
		this.options = options;

		this.carouselItems = this.props.items.length ? this.props.items : this.props.children;
	}

	onClickPrev = () => {
		this.flkty.previous();
	};

	onClickNext = () => {
		this.flkty.next();
	};

	onGoToSlide = index => {
		if (this.flkty) {
			this.flkty.select(index);
		}
	};

	onResize = () => {
		const self = this;

		if (this.options.draggable) {
			setTimeout(() => {
				// if the lides length is 1 (nothing to slide) disable dragging
				if (self.flkty.slides && self.flkty.slides.length === 1) {
					self.flkty.unbindDrag();
				} else {
					self.flkty.bindDrag();
				}
			}, 2);
		}

		if (this.flkty.slides) {
			setTimeout(() => {
				self.setState({
					dots: this.flkty.slides.length
				});
			}, 300);
		}
	};

	componentDidMount() {
		const {onChange, isLazyLoad, noMobileCarousel} = this.props;

		if (this.carouselItems.length < 1 || noMobileCarousel || !this.id) {
			return false;
		}

		if (!SERVER) {
			const flkty = new Flickity(`#${this.id}`, this.options);
			this.flkty = flkty;

			// bind events
			flkty.on('settle', () => {
				if (isLazyLoad && onChange) {
					onChange(flkty.selectedIndex, flkty.cells.length);
				}
			});

			flkty.on('select', () => {
				if (!isLazyLoad && onChange) {
					onChange(flkty.selectedIndex, flkty.cells.length);
				}
				this.setState({
					currentSlide: this.flkty.selectedIndex
				});
			});

			if (typeof window === 'object') {
				window.addEventListener('resize', this.onResize);
			}

			if (flkty.slides) {
				this.setState({
					// adding because of potential hash navigation
					currentSlide: flkty.options.initialIndex || 0,
					dots: flkty.slides.length
				});
			}

			this.onResize();
		}
	}

	get current() {
		return this.state.currentSlide;
	}

	scrollTo(index) {
		this.onGoToSlide(index);
	}

	set selected(index) {
		this.scrollTo(index);
	}

	stopAutoPlay() {
		this.flkty.stopPlayer();
	}

	append(items) {
		const newitems = [];
		for (const item of items) {
			const slide = document.createElement('div');
			slide.className = `${this.baseClass}__cell`;
			slide.innerHTML = `<div>${item}</div>`;
			newitems.push(slide);
		}

		this.flkty.append(newitems);
	}

	_destroyCarousel() {
		this.flkty.destroy();
	}

	disableDrag() {
		this.flkty.unbindDrag();
	}

	enableDrag() {
		this.flkty.bindDrag();
	}

	render() {
		const {prevText, nextText, doFade, arrowStyle, hasControlOverlap} = this.props;

		const {classes, hasCustomControls, itemsPerRow} = this.props;

		const {currentSlide} = this.state;

		const self = this;

		// items
		this.items = [];
		this.themes = [];

		if (this.carouselItems) {
			const itemCount = this.carouselItems.length;
			const rowCount = Math.ceil(itemCount / itemsPerRow);

			this.items = this.carouselItems.map((item, i) => {
				const isLast = i >= (rowCount - 1) * itemsPerRow;
				const isLastClass = isLast ? 'last' : '';

				// TODO: check what type item is

				if (item.props && item.props.theme) {
					this.themes[i] = item.props.theme;
				}

				const id = item.props && item.props._id ? item.props._id : null;

				return (
					<div className={`${self.baseClass}__cell ${isLastClass}`} key={i} id={id}>
						{item}
					</div>
				);
			});
		}

		const atts = {
			className: getModifiers(this.baseClass, [
				doFade ? ' fade' : null,
				hasControlOverlap ? 'overlap-controls' : null
			]),
			'data-theme': this.themes[currentSlide] || undefined
		};

		if (classes) {
			atts.className += ` ${classes}`;
		}

		const dots = [];

		if (this.state.dots > 1) {
			for (let i = 0; i < this.state.dots; i++) {
				const selected = this.state.currentSlide === i ? 'selected' : undefined;
				dots.push(
					<li key={i} className={selected} onClick={() => this.onGoToSlide(i)}>
						{i + 1}
					</li>
				);
			}
		}

		return (
			<div {...atts}>
				<div className={`${this.baseClass}__wrapper`}>
					<div id={this.id} className={`${this.baseClass}__carousel`}>
						{this.items}
					</div>
				</div>
				{hasCustomControls && this.id && (this.custom.pageDots || this.custom.prevNextButtons) && (
					<div className={`${self.baseClass}__controls`}>
						{this.custom.prevNextButtons && (
							<button
								type="button"
								aria-label={prevText}
								className={`${self.baseClass}__nav prev`}
								onClick={this.onClickPrev}
							>
								{arrowStyle && (
									<svg className={`${this.baseClass}__arrow`} viewBox="0 0 100 100">
										<path d={arrowStyle} />
									</svg>
								)}

								<Text content={prevText} className={`${self.baseClass}__label`} />
							</button>
						)}
						{this.custom.pageDots && dots.length > 1 && (
							<ul className={`${this.baseClass}__bullets`}>{dots}</ul>
						)}
						{this.custom.prevNextButtons && (
							<button
								type="button"
								aria-label={nextText}
								className={`${self.baseClass}__nav next`}
								onClick={this.onClickNext}
							>
								<Text content={nextText} className={`${self.baseClass}__label`} />

								{arrowStyle && (
									<svg className={`${this.baseClass}__arrow`} viewBox="0 0 100 100">
										<g transform="translate(100, 100) rotate(180)">
											<path d={arrowStyle} />
										</g>
									</svg>
								)}
							</button>
						)}
					</div>
				)}
			</div>
		);
	}
}

Carousel.defaultProps = {
	hasControlOverlap: false,
	children: '',
	id: null,
	items: [],
	prevText: 'Prev',
	nextText: 'Next',
	hasCustomControls: true,
	options: {},
	doFade: false,
	arrowStyle: Shapes.CHEVERON_LEFT,
	onChange: null,
	classes: '',
	isLazyLoad: false,
	itemsPerRow: 0
};

Carousel.propTypes = {
	hasControlOverlap: PropTypes.bool,
	arrowStyle: PropTypes.string,
	children: PropTypes.node,
	classes: PropTypes.string,
	doFade: PropTypes.bool,
	hasCustomControls: PropTypes.bool,
	id: PropTypes.string.isRequired,
	isLazyLoad: PropTypes.bool,
	items: PropTypes.array,
	itemsPerRow: PropTypes.number,
	nextText: PropTypes.string,
	noMobileCarousel: PropTypes.bool,
	options: PropTypes.object,
	prevText: PropTypes.string,
	onChange: PropTypes.func
};

export default Carousel;
